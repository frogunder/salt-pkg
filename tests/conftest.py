import logging
import pathlib
import pprint
import re
import shutil

import distro
import pytest

import tests.support.helpers

TESTS_DIR = pathlib.Path(__file__).resolve().parent
CODE_DIR = TESTS_DIR.parent
ARTIFACTS_DIR = CODE_DIR / "artifacts"

log = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def version():
    """
    get version number from artifact
    """
    _version = ""
    for artifact in ARTIFACTS_DIR.glob("**/*.*"):
        _version = re.search(r"([0-9].*)(\-[0-9].el|\+ds|\-[0-9].am)", artifact.name)
        if _version:
            _version = _version.groups()[0].replace("_", "-").replace("~", "")
            break
    return _version


@pytest.fixture(autouse=True, scope="package")
def install_salt():
    """
    Install Salt
    """
    # todo make the pkg path configurable
    distro_id = distro.id().lower()
    salt_pkgs = [
        "salt-api",
        "salt-syndic",
        "salt-ssh",
        "salt-master",
        "salt-cloud",
        "salt-minion",
    ]

    pkgs = []
    if distro_id in ["centos", "redhat", "amzn"]:
        salt_pkgs.append("salt")
        pkg_mngr = "yum"
        rm_pkg = "remove"

    elif distro_id in ["ubuntu", "debian"]:
        salt_pkgs.append("salt-common")
        pkg_mngr = "apt-get"
        rm_pkg = "purge"

    for files in ARTIFACTS_DIR.glob("**/*.*"):
        files = str(files)
        if re.search("salt(.*)(x86_64|all|amd64).(rpm|deb)", files):
            pkgs.append(files)

    pkg_install = [pkg_mngr, "install", "-y"]
    pkg_remove = [pkg_mngr, rm_pkg, "-y"]

    log.debug("Installing packages:\n%s", pprint.pformat(pkgs))
    ret = tests.support.helpers.run(pkg_install + pkgs)
    assert ret["retcode"] == 0
    yield
    # remove packages
    log.debug("Un-Installing packages:\n%s", pprint.pformat(pkgs))
    ret = tests.support.helpers.run(pkg_remove + salt_pkgs)
    assert ret["retcode"] == 0


@pytest.fixture(scope="module")
def sls():
    """
    Add an sls file
    """
    file_root = pathlib.Path("/srv/salt")
    file_root.mkdir(mode=0o777, parents=True)
    test_sls = file_root / "test.sls"
    with open(test_sls, "w") as _fh:
        _fh.write(
            """
            test_foo:
              test.succeed_with_changes:
                  - name: foo
        """
        )
    try:
        yield test_sls
    finally:
        shutil.rmtree(file_root.parent)


# todo: allow someone to customize config before startup of master
@pytest.fixture(scope="module")
def master_config(contents=None):
    """
    Add the Salt Master config
    """

    def _config(contents):
        tests.support.helpers.write_config("master", contents=contents)

    return _config


# todo: allow someone to customize config before startup of minion
@pytest.fixture(scope="module")
def minion_config():
    """
    Add the Salt Minion config
    """
    contents = {"master": "127.0.0.1", "id": "pkg_tests"}
    tests.support.helpers.write_config("minion", contents=contents)

    def _config(id="pkg_tests", master="127.0.0.1", contents=None):
        """
        adding custom content to config
        """
        if not contents:
            contents = {}
        default = {"master": master, "id": id}
        contents.update(default)
        tests.support.helpers.write_config("minion", contents=contents)

    return _config


@pytest.fixture(scope="module")
def api_config():
    """
    Add the Salt Api config
    """
    contents = {
        "rest_cherrypy": {"port": 8000, "disable_ssl": True},
        "external_auth": {"auto": {"saltdev": [".*"]}},
    }
    tests.support.helpers.write_config(
        pathlib.Path("master.d/api.conf"), contents=contents
    )


@pytest.fixture(scope="module")
def start_master(master_config):
    """
    Start up a master
    """
    try:
        tests.support.helpers.start_service("salt-master")
        yield
    finally:
        tests.support.helpers.stop_service("salt-master")


@pytest.fixture(scope="module")
def start_minion(minion_config):
    """
    Start up a minion
    """
    try:
        tests.support.helpers.start_service("salt-minion")
        yield
    finally:
        tests.support.helpers.stop_service("salt-minion")


@pytest.fixture(scope="module")
def master_minion(start_master, start_minion):
    """
    Start both a master and a minion and accept keys
    """
    # accept the key after services started
    try:
        accept_key = tests.support.helpers.run(["salt-key", "-a", "pkg_tests", "-y"])
        assert accept_key["retcode"] == 0
        yield
    finally:
        delete_key = tests.support.helpers.run(["salt-key", "-d", "pkg_tests", "-y"])
        assert delete_key["retcode"] == 0


@pytest.fixture(scope="module")
def test_account():
    api_usr = tests.support.helpers.TestUser()
    try:
        api_usr.add_user()
        # setup salt-api user
        yield api_usr
    finally:
        api_usr.remove_user()


@pytest.fixture(scope="module")
def salt_api(api_config, master_minion, test_account):
    """
    start up and configure salt_api
    """
    try:
        tests.support.helpers.start_service("salt-api")

        yield test_account
    finally:
        tests.support.helpers.stop_service("salt-api")
