import pytest

import tests.support.helpers


@pytest.mark.parametrize(
    "cmd",
    [
        "salt",
        "salt-api",
        "salt-call",
        "salt-cloud",
        "salt-cp",
        "salt-key",
        "salt-master",
        "salt-minion",
        "salt-proxy",
        "salt-run",
        "salt-ssh",
        "salt-syndic",
        "spm",
    ],
)
def test_help(cmd):
    """
    Test --help works for all salt cmds
    """
    ret = tests.support.helpers.run([cmd, "--help"])
    assert "Usage" in ret["stdout"]
    assert ret["retcode"] == 0
