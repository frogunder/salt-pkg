import crypt
import json
import logging
import os
import pathlib
import pprint
import pwd
import subprocess
import time

import attr
import requests
import yaml

log = logging.getLogger(__name__)


def run(cmd, minion="local"):
    if not isinstance(cmd, list):
        raise TypeError("Expected cmd to be a list")
    ret = {}
    proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    ret["stdout"] = proc.stdout.decode()
    ret["stderr"] = proc.stderr.decode()
    if ret["stdout"]:
        try:
            ret["stdout"] = json.loads(ret["stdout"])[minion]
        except json.decoder.JSONDecodeError:
            pass
    ret["retcode"] = proc.returncode
    return ret


def salt_call(args):
    """
    Run salt-call commands
    """
    cmd = ["salt-call", "--out=json"]
    cmd += args
    ret = run(cmd)
    return ret


def salt_call_local(args):
    """
    Run salt-call --local commands
    """
    cmd = ["salt-call", "--local", "--out=json"]
    cmd += args
    ret = run(cmd)
    return ret


def salt_minion(args):
    """
    Run salt commands
    """
    cmd = ["salt", "*"]
    cmd += args
    cmd.append("--out=json")
    ret = run(cmd, minion="pkg_tests")
    return ret


def api(data, username="saltdev", password="saltdev"):
    """
    Run salt-api cmds
    """
    api_uri = "http://localhost:8000"
    session = requests.Session()
    auth = {"username": username, "password": password, "eauth": "auto", "out": "json"}
    data = {**auth, **data}

    resp = session.post(f"{api_uri}/run", data=data).json()

    minion = next(iter(resp["return"][0]))
    return resp["return"][0][minion]


### manage configs ###


def write_config(config, contents=None):
    """
    write to a config file
    """
    if not contents:
        contents = {}
    conf = pathlib.Path("/etc/salt/", config)
    with open(conf, "w") as _fh:
        yaml.safe_dump(contents, _fh)


### manage services ###


def start_service(service):
    # todo: verify service is up instead of time.sleep
    start = run(["systemctl", "start", service])
    assert start["retcode"] == 0
    time.sleep(5)
    status = run(["systemctl", "status", service])
    log.debug(f"Systemd service status for {service}:\n{pprint.pformat(status)}")


def stop_service(service):
    # todo: verify service is down
    stop = run(["systemctl", "stop", service])
    assert stop["retcode"] == 0


@attr.s
class TestUser:
    """
    Add a test user
    """

    username = attr.ib(default="saltdev")
    password = attr.ib(default="saltdev")
    _pw_record = attr.ib(init=False, repr=False, default=None)

    def add_user(self):
        log.debug("Adding system account %r", self.username)
        hash_passwd = crypt.crypt(self.password, crypt.mksalt(crypt.METHOD_SHA512))
        assert salt_call_local(["user.add", self.username])
        assert salt_call_local(["shadow.set_password", self.username, hash_passwd])
        assert self.username in salt_call_local(["user.list_users"])["stdout"]

    def remove_user(self):
        log.debug("Removing system account %r", self.username)
        assert salt_call_local(["user.delete", self.username, "remove=True"])

    @property
    def pw_record(self):
        if self._pw_record is None:
            self._pw_record = pwd.getpwnam(self.username)
        return self._pw_record

    @property
    def uid(self):
        return self.pw_record.pw_uid

    @property
    def gid(self):
        return self.pw_record.pw_gid

    @property
    def env(self):
        environ = os.environ.copy()
        environ["LOGNAME"] = environ["USER"] = self.username
        environ["HOME"] = self.pw_record.pw_dir
        return environ
