import os
import pathlib
import shutil
import subprocess

from tests.support.helpers import salt_call_local


def test_pip_install():
    """
    Test pip.install and ensure
    module can use installed library
    """
    dep = "PyOpenSSL"

    try:
        install = salt_call_local(["pip.install", dep])
        assert install["retcode"] == 0

        use_lib = salt_call_local(["tls.create_ca", "test"])
        assert "Created Private Key" in use_lib["stdout"]
    finally:
        cert_path = pathlib.Path(
            salt_call_local(["tls.cert_base_path"])["stdout"], "test"
        )
        shutil.rmtree(cert_path)
        ret = salt_call_local(["pip.uninstall", dep])
        assert ret["stdout"]["retcode"] == 0


def demote(user_uid, user_gid):
    def result():
        os.setgid(user_gid)
        os.setuid(user_uid)

    return result


def test_pip_non_root(test_account):
    pypath = pathlib.Path(f"{os.sep}opt", "saltstack", "salt", "pypath")
    # Let's make sure pypath does not exist
    shutil.rmtree(pypath)

    assert not pypath.exists()
    # We should be able to issue a --help without being root
    ret = subprocess.run(
        ["salt", "--help"],
        preexec_fn=demote(test_account.uid, test_account.gid),
        env=test_account.env,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=False,
        universal_newlines=True,
    )
    assert ret.returncode == 0, ret.stderr
    assert "Usage" in ret.stdout
    assert not pypath.exists()

    # Try to pip install something, should fail
    ret = subprocess.run(
        ["salt", "pip", "install", "pep8"],
        preexec_fn=demote(test_account.uid, test_account.gid),
        env=test_account.env,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=False,
        universal_newlines=True,
    )
    assert ret.returncode == 1, ret.stderr
    assert (
        "The path '/opt/saltstack/salt/pypath' does not exist or could not be created."
        in ret.stderr
    )
    assert not pypath.exists()

    # Let tiamat-pip create the pypath directory for us
    ret = subprocess.run(
        ["salt", "pip", "install", "-h"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=False,
        universal_newlines=True,
    )
    assert ret.returncode == 0, ret.stderr

    # Now, we should still not be able to install as non-root
    ret = subprocess.run(
        ["salt", "pip", "install", "pep8"],
        preexec_fn=demote(test_account.uid, test_account.gid),
        env=test_account.env,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=False,
        universal_newlines=True,
    )
    assert ret.returncode != 0, ret.stderr

    # But we should be able to install as root
    ret = subprocess.run(
        ["salt", "pip", "install", "pep8"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=False,
        universal_newlines=True,
    )
    assert ret.returncode == 0, ret.stderr
