# salt-pkg: Building Salt for Your Consumption

`salt-pkg` builds and packages salt, using tools like [Tiamat](https://gitlab.com/saltstack/pop/tiamat), for a variety of systems.

<!-- TOC -->

- [About The Project](#about-the-project)
- [Using Tiamat builds of salt](#using-tiamat-builds-of-salt)
    - [Installing debian-based packages with apt](#installing-debian-based-packages-with-apt)
    - [Installing RHEL/CentOS-based packages with yum](#installing-rhelcentos-based-packages-with-yum)
        - [CentOS 7](#centos-7)
        - [CentOS 8](#centos-8)
        - [Amazon Linux 2](#amazon-linux-2)
        - [Oracle Linux 7](#oracle-linux-7)
    - [How Tiamat-generated salt works](#how-tiamat-generated-salt-works)

<!-- /TOC -->


<!-- ABOUT THE PROJECT -->
## About The Project

> TODO

## Using Tiamat builds of salt

Tiamat-generated builds of `salt` are in **beta**, and are ready for testing.

### Installing debian-based packages with `apt`

If wanting to install on debian/Ubuntu-based systems using the `apt` package manager, follow the directions for the appropriate OS target.

Supported codenames:

- bionic
- buster
- focal
- stretch
- xenial

```bash
CODENAME=$(grep VERSION_CODENAME /etc/os-release | cut -d= -f2)
wget -O - https://artifactory.saltstack.net/artifactory/api/gpg/key/public | apt-key --keyring /etc/apt/trusted.gpg.d/saltstack.gpg add -
echo 'deb https://artifactory.saltstack.net/artifactory/open-debian-staging '"${CODENAME}"' main' > /etc/apt/sources.list.d/saltstack.list
```

```bash
apt-get update

# Install salt packages as normal
# Example for salt-minion
apt-get install --yes salt-minion
```

### Installing RHEL/CentOS-based packages with `yum`

If wanting ot install on RHEL/CentOS-based systems using the `yum` package manager, follow the directions for the appropriate OS target.

#### CentOS 7

```bash
rpm --import https://artifactory.saltstack.net/artifactory/api/gpg/key/public

# Create or overwrite SaltStack repo file
{
  echo '[saltstack-repo]'
  echo 'name=SaltStack Tiamat Staging Channel for RHEL/CentOS 7'
  echo 'baseurl=https://artifactory.saltstack.net/artifactory/open-rpm-staging/centos/7/x86_64/'
  echo 'skip_if_unavailable=True'
  echo 'gpgcheck=0'
  echo 'gpgkey=https://artifactory.saltstack.net/artifactory/api/gpg/key/public'
  echo 'enabled=1'
  echo 'enabled_metadata=1'
  echo 'repo_gpgcheck=1'
} > /etc/yum.repos.d/saltstack.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

#### CentOS 8

```bash
rpm --import https://artifactory.saltstack.net/artifactory/api/gpg/key/public

# Create or overwrite SaltStack repo file
{
  echo '[saltstack-repo]'
  echo 'name=SaltStack Tiamat Staging Channel for RHEL/CentOS 8'
  echo 'baseurl=https://artifactory.saltstack.net/artifactory/open-rpm-staging/centos/8/x86_64/'
  echo 'skip_if_unavailable=True'
  echo 'gpgcheck=0'
  echo 'gpgkey=https://artifactory.saltstack.net/artifactory/api/gpg/key/public'
  echo 'enabled=1'
  echo 'enabled_metadata=1'
  echo 'repo_gpgcheck=1'
} > /etc/yum.repos.d/saltstack.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

#### Amazon Linux 2

```bash
rpm --import https://artifactory.saltstack.net/artifactory/api/gpg/key/public

# Create or overwrite SaltStack repo file
{
  echo '[saltstack-repo]'
  echo 'name=SaltStack Tiamat Staging Channel for Amazon Linux 2'
  echo 'baseurl=https://artifactory.saltstack.net/artifactory/open-rpm-staging/amazon/2/x86_64/'
  echo 'skip_if_unavailable=True'
  echo 'gpgcheck=0'
  echo 'gpgkey=https://artifactory.saltstack.net/artifactory/api/gpg/key/public'
  echo 'enabled=1'
  echo 'enabled_metadata=1'
  echo 'repo_gpgcheck=1'
} > /etc/yum.repos.d/saltstack.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

#### Oracle Linux 7

```bash
rpm --import https://artifactory.saltstack.net/artifactory/api/gpg/key/public

# Create or overwrite SaltStack repo file
{
  echo '[saltstack-repo]'
  echo 'name=SaltStack Tiamat Staging Channel for Oracle Linux 7'
  echo 'baseurl=https://artifactory.saltstack.net/artifactory/open-rpm-staging/oracle/7/x86_64/'
  echo 'skip_if_unavailable=True'
  echo 'gpgcheck=0'
  echo 'gpgkey=https://artifactory.saltstack.net/artifactory/api/gpg/key/public'
  echo 'enabled=1'
  echo 'enabled_metadata=1'
  echo 'repo_gpgcheck=1'
} > /etc/yum.repos.d/saltstack.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

### How Tiamat-generated `salt` works

When installing from `open-*-staging` repositories (following the steps above), the following is the result:

- **These are nightly builds, with a version output representing the current build installed.**
- `salt-master`, `salt-minion`, and other `salt` calls are really just bash-shell scripts that call out to `/opt/saltstack/salt/run/run <master|minion|call|etc.>` (with `run` being the Tiamat-generated, [PyInstaller](https://www.pyinstaller.org/) launcher of `salt`).

All of `salt` is contained within `/opt/saltstack/salt/`, with the primary calls being launched from `/opt/saltstack/salt/run/run`. `run` takes args to provide functionality similar to how `salt-master`, `salt-minion`, `salt-cloud`, etc. all worked.

If calling directly from the binary, it would work like `run <type> <args>`:

- `run master <args>` == `salt-master <args>`
- `run minion <args>` == `salt-minion <args>`

#### Single binary availability

Currently, Tiamat builds of `salt` are single directory builds. The debian and rpm packages provide `salt` with a single directory of everything stored in `/opt/saltstack/salt`. Single binary builds will eventually be made available once issues such as [#27](https://gitlab.com/saltstack/open/salt-pkg/-/issues/27) are resolved.
