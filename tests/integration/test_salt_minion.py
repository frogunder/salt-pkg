from tests.support.helpers import salt_minion


def test_salt_minion_ping(master_minion):
    """
    Test running a command against a targeted minion
    """
    ret = salt_minion(["test.ping"])
    assert ret["stdout"]
