#!/bin/bash
yum install -y vim-enhanced gcc rpm-build rpm-devel rpmlint make python python3 python3-devel python3-pip bash coreutils diffutils patch rpmdevtools git redhat-rpm-config rpmdevtools openssl-devel

pip3 install tiamat bodger

git clone https://gitlab.com/saltstack/pop/pkgr.git /pkgr

pip3 install -e /pkgr
