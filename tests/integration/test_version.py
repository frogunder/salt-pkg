import tests.support.helpers


def test_salt_version(version):
    """
    Test version outputed from salt --version
    """
    ret = tests.support.helpers.run(["salt", "--version"])
    assert ret["stdout"].strip() == f"salt {version}"
