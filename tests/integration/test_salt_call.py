from tests.support.helpers import salt_call
from tests.support.helpers import salt_call_local


def test_salt_call_local(master_minion):
    """
    Test salt-call --local test.ping
    """
    ret = salt_call_local(["test.ping"])
    assert ret["stdout"]


def test_salt_call(master_minion):
    """
    Test salt-call test.ping
    """
    ret = salt_call(["test.ping"])
    assert ret["stdout"]


def test_sls(sls, master_minion):
    """
    Test calling a sls file
    """
    ret = salt_call(["state.apply", "test"])
    sls_ret = ret["stdout"][next(iter(ret["stdout"]))]
    assert sls_ret["changes"]["testing"]["new"] == "Something pretended to change"
